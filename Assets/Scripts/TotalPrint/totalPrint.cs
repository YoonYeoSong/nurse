﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class totalPrint : MonoBehaviour {

    public GameObject IdnumtxtPrint;
    public GameObject DatePrint;
    public GameObject LevelPrint;
    public GameObject RoomNumberPrint;
    public GameObject OkPrint;
	public Text scorePrint,timePrint;
    
    //public bool IsTrue = false;
    

    public void OnClicked()
    {
        
    }
    void loadLevel()
    {
        Application.LoadLevel("LevelSelect");
    }

    // Use this for initialization
    void Start () {
		if (Levelbutton.isTrueLow == true)
		{
			IdnumtxtPrint.GetComponent<Text>().text ="학번 :" + DB.idnum;
			LevelPrint.GetComponent<Text>().text = "Level"+Levelbutton.LevelName01;
			DatePrint.GetComponent<Text>().text = DateTime.Now.ToString("yy/MM/dd hh:mm:ss");
			RoomNumberPrint.GetComponent<Text>().text = Levelbutton.roomNumber01;
			scorePrint.text = GM.score.ToString();
			timePrint.text = GM.minTime.ToString()+":"+GM.timeSecond.ToString();
            OkPrint.GetComponent<Text>().text = GM.Completeprint;
			//Application.LoadLevel("LevelSelect");
			Levelbutton.imgFinalActive = true;
			Levelbutton.backButtonActive = true;
			Levelbutton.L01_01 = false;
			Levelbutton.L01_02 = false;
			Levelbutton.L02_01 = false;
			Levelbutton.L02_02 = false;
			Levelbutton.L02_03 = false;
			Levelbutton.L03_01 = false;
			Levelbutton.L03_02 = false;
			Levelbutton.L03_03 = false;
			//Invoke("loadLevel", 5);
		}
		else if(Levelbutton.isTrueMid == true)
		{
			IdnumtxtPrint.GetComponent<Text>().text = DB.idnum;
			LevelPrint.GetComponent<Text>().text = "Level"+Levelbutton.LevelName01;
			DatePrint.GetComponent<Text>().text = DateTime.Now.ToString("yyyy-MM-dd");
			RoomNumberPrint.GetComponent<Text>().text = Levelbutton.roomNumber01;
			scorePrint.text = GM.score.ToString();
			timePrint.text = GM.minTime.ToString()+":"+GM.timeSecond.ToString();
            OkPrint.GetComponent<Text>().text = GM.Completeprint;
            //Application.LoadLevel("LevelSelect");
            Levelbutton.imgFinalActive = true;
			Levelbutton.backButtonActive = true;
			Levelbutton.L01_01 = false;
			Levelbutton.L01_02 = false;
			Levelbutton.L02_01 = false;
			Levelbutton.L02_02 = false;
			Levelbutton.L02_03 = false;
			Levelbutton.L03_01 = false;
			Levelbutton.L03_02 = false;
			Levelbutton.L03_03 = false;
			//Invoke("loadLevel", 5);
		}
		else if(Levelbutton.isTrueHigh == true)
		{
			IdnumtxtPrint.GetComponent<Text>().text = DB.idnum;
			LevelPrint.GetComponent<Text>().text = "Level"+Levelbutton.LevelName01;
			DatePrint.GetComponent<Text>().text = DateTime.Now.ToString("yy/MM/dd hh:mm:ss");
			RoomNumberPrint.GetComponent<Text>().text = Levelbutton.roomNumber01;
			scorePrint.text = GM.score.ToString();
			timePrint.text = GM.minTime.ToString()+":"+GM.timeSecond.ToString();
            OkPrint.GetComponent<Text>().text = GM.Completeprint;

            //Application.LoadLevel("LevelSelect");
            Levelbutton.imgFinalActive = true;
			Levelbutton.backButtonActive = true;
			Levelbutton.L01_01 = false;
			Levelbutton.L01_02 = false;
			Levelbutton.L02_01 = false;
			Levelbutton.L02_02 = false;
			Levelbutton.L02_03 = false;
			Levelbutton.L03_01 = false;
			Levelbutton.L03_02 = false;
			Levelbutton.L03_03 = false;
			//Invoke("loadLevel", 5);
		}

        //send result to server.
        int diff = Convert.ToInt16(Levelbutton.LevelName01);
        int level = Convert.ToInt16(Levelbutton.roomNumber01);
        string idnum = DB.idnum;
        int score = (int)GM.score;
        int duringtime = 60 + (30*diff) -(GM.minTime * 60 + GM.timeSecond);
        int isSuccess = 1;
        string token = DB.token;

        //reset variables  
        GM.Completeprint = "";
        GM.score = 100;

        StartCoroutine(sendValues(diff, level, idnum, score, duringtime, isSuccess, token));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public IEnumerator sendValues(int diff, int level, string idnum, int score, int duringtime, int suc, string token)
    {
        WWWForm cForm = new WWWForm();
        cForm.AddField("idnum", idnum);
        cForm.AddField("method", "ud");
        cForm.AddField("token", token);
        Debug.Log(token);
        cForm.AddField("diff", diff);
        cForm.AddField("level", level);
        cForm.AddField("score", score);
        cForm.AddField("success", suc);
        cForm.AddField("duration", duringtime);
        WWW wwwUrl = new WWW("http://ciiwolstudio.com/nursetrain/communicate.php", cForm);
        yield return wwwUrl;

        if (wwwUrl.error == null)
        {
            Debug.Log(wwwUrl.text);
            Invoke("loadLevel", 5);
        }
        else
        {
            Debug.Log("WWW error :" + wwwUrl.error);
        }
    }
}
