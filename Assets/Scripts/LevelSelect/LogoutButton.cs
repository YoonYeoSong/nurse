﻿using UnityEngine;
using System.Collections;

public class LogoutButton : MonoBehaviour {

    public IEnumerator WaitForRequest(string _addres)
    {
        WWWForm cForm = new WWWForm();
        cForm.AddField("idnum", DB.idnum);
        cForm.AddField("method", "lo");
        WWW wwwUrl = new WWW(_addres, cForm);
        yield return wwwUrl;

        if (wwwUrl.error == null) // 로그아웃시
        {
            Debug.Log(wwwUrl.text);
            Application.Quit();
            //Application.LoadLevel("MainLogin");
			Levelbutton.imgFinalActive = false;
			Levelbutton.backButtonActive = false;
            Levelbutton.imageLevelActive = true;
            Levelbutton.LevelLowActive = true;
            Levelbutton.LevelMidActive = true;
            Levelbutton.LevelHightActive = true;
            Levelbutton.LowRoomTotalActive = false;
            Levelbutton.MidRoomTotalActive = false;
            Levelbutton.HighRoomTotalActive = false;
            Levelbutton.isTrueHigh = false;
            Levelbutton.isTrueLow = false;
            Levelbutton.isTrueMid = false;
        }
        else
        {
            Debug.Log("WWW error :" + wwwUrl.error);
        }

    }

    public static void OnHomeButton()
    {

    }

    public void OnClicked()
    {
        StartCoroutine(WaitForRequest("http://ciiwolstudio.com/nursetrain/communicate.php"));
    }
	// Use this for initialization
	void Start () {
        Screen.orientation = ScreenOrientation.Portrait; // 세로 방향전환
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
