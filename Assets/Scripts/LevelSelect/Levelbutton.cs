﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Levelbutton : MonoBehaviour {
    public GameObject newBtn;
    public GameObject Canv; // canvus
    //public GameObject LevelButton; //레벨버튼과 레벨선택이미지 총포함
    public GameObject LevelLow; // 레벨버튼 1번
    public GameObject LevelMid; // 레벨버튼 2번
    public GameObject LevelHigh; // 레벨버튼 3번
    public GameObject imageRoom; // 입원실 고르기 텍스처
    public GameObject imageLevel; // 레벨 고르기 텍스처
    public GameObject imageFinal; // 겜이 끝나고 레벨 밑에 생기는 텍스처
    public GameObject LowRoomTotal; // 레벨 1의 입원실 전체 
	public GameObject BackButton; // 겜이끝나고  imageFinal 밑에 생기는 돌아가기 버튼
	public GameObject MidRoomTotal; // 레벨 2의 입원실 전체
    public GameObject HighRoomTotal; // 레벨 3의 입원실 전체

    //레벨 버튼 Active
    public static bool LevelLowActive = true;
    public static bool LevelMidActive = true;
    public static bool LevelHightActive = true;

    public static bool imgFinalActive = false;
    public static bool imageRoomActive = false;
    public static bool backButtonActive = false;
    public static bool imageLevelActive = true;

    //레벨에 따른 입원실 Active
    public static bool LowRoomTotalActive = false;
    public static bool MidRoomTotalActive = false;
    public static bool HighRoomTotalActive = false;


    public static bool isTrueLow = false;
	public static bool isTrueMid = false;
	public static bool isTrueHigh = false;

    public static string LevelName01;  
	public static string roomNumber01;
    public static string roomNumber02;

	public static bool L01_01,L01_02, L02_01,L02_02,L02_03, L03_01,L03_02,L03_03;
    //public static string roomNumber02 = "2 호실";
	//public static string LevelName02 = "Level02";
	//public static string levelName03 = "Level03";
	// public GameObject LowRoom1;
	// public GameObject LowRoom2;
	//public GameObject LowRoom3;
	//public GameObject LowRoom4;
   // public int i;
   // public int j;
    //public Sprite[] LowLevelsprite = new Sprite[4];
    //public Sprite[] MidLevelsprite = new Sprite[6];
    public void OnHomeButton()
    {
        LevelLow.gameObject.SetActive(true);
        LevelMid.gameObject.SetActive(true);
        LevelHigh.gameObject.SetActive(true);
        imageFinal.gameObject.SetActive(false);
        BackButton.gameObject.SetActive(false);
        LowRoomTotal.gameObject.SetActive(false);
        MidRoomTotal.gameObject.SetActive(false);
        HighRoomTotal.gameObject.SetActive(false);
		Levelbutton.L01_01 = false;
		Levelbutton.L01_02 = false;
		Levelbutton.L02_01 = false;
		Levelbutton.L02_02 = false;
		Levelbutton.L02_03 = false;
		Levelbutton.L03_01 = false;
		Levelbutton.L03_02 = false;
		Levelbutton.L03_03 = false;
        Application.LoadLevel("LevelSelect");
    }

    public void OnClickedLow()
    {
        imageFinal.SetActive(false);
        BackButton.SetActive(false);
        LevelName01 = "01";
        isTrueLow = true;
        imageLevel.gameObject.SetActive(false);// 1번 레벨 클릭시 이미지 레벨을 끈다.
        imageRoom.gameObject.SetActive(true); // 입원실선택 이미지 을 띄운다.

        LevelMid.gameObject.SetActive(false); // 1번 레벨 끈다.
        LevelHigh.gameObject.SetActive(false);// 2번 레벨
        LevelLow.gameObject.SetActive(false); // 3번 레벨
		
        if (isTrueLow == true) {              //1번레벨이 true일경우
            LowRoomTotal.gameObject.SetActive(true); // 1번레벨의 입원실의 창을 띄운다.
        //Canvas canvas = NB.AddComponent<Canvas>();
		//canvas.renderMode = RenderMode.WorldSpace;

            //LowRoom1.gameObject.SetActive(true);
           // LowRoom2.gameObject.SetActive(true);
           // LowRoom3.gameObject.SetActive(true);
           // LowRoom4.gameObject.SetActive(true);  
            /*
            for (i=0; i < 4; i+=30)
            {
                roomPanel.gameObject.SetActive(true);
                GameObject newBtn1 = (GameObject)Instantiate(newBtn, transform.position*i, transform.rotation);
                newBtn1.transform.SetParent(Canv.transform);
                for(j=0; j < 4; j++)
                {
                   newBtn1.gameObject.GetComponent<SpriteRenderer>().sprite = LowLevelsprite[j];
                }
            }
            */
        }
    }

	public void OnLowRoom1(){ //1번 레벨의 1호실을 클릭시
		roomNumber01 = "1"; // 1이라는 숫자를 알려준다.
		L01_01 = true;
		Application.LoadLevel ("LevelMain");
	}
	public void OnLowRoom2(){ // 1번레벨의 2호실을 클릭시
		roomNumber01 = "2";
		L01_02 = true;
		Application.LoadLevel ("LevelMain");
	}
	public void OnLowRoom3(){ // 1번 레벨의 3호실을 클릭시
		roomNumber01 = "3";
	}
	public void OnLowRoom4(){ // 1번 레벨의 4호실을 클릭시
		roomNumber01 = "4";
	}

    public void OnClickedMid() // 2번 레벨 클릭시
    {
        imageFinal.SetActive(false);
        BackButton.SetActive(false);
        isTrueLow = false; // 1번 레벨은 끈다.
        isTrueHigh = false; // 3번레벨도 끈다.
        isTrueMid = true; // 2번레벨 킨다.
        Debug.Log("2");
        // DateTime.Now.ToString();
        LevelName01 = "02";

        imageLevel.gameObject.SetActive(false);
        imageRoom.gameObject.SetActive(true);

        LevelMid.gameObject.SetActive(false);
        LevelHigh.gameObject.SetActive(false);
        LevelLow.gameObject.SetActive(false);

        if (isTrueMid == true)
        {
            MidRoomTotal.gameObject.SetActive(true);

        }
    }

    public void OnMidRoom1()
    { //2번 레벨의 1호실을 클릭시
        roomNumber01 = "1"; // 1이라는 숫자를 알려준다.
		L02_01 = true;
		Application.LoadLevel ("LevelMain");
    }
    public void OnMidRoom2()
    { // 2번레벨의 2호실을 클릭시
        roomNumber01 = "2";
		L02_02 = true;
		Application.LoadLevel ("LevelMain");
    }
    public void OnMidRoom3()
    { // 2번 레벨의 3호실을 클릭시
		L02_03 = true;
		Application.LoadLevel ("LevelMain");
    }
    public void OnMidRoom4()
    { // 2번 레벨의 4호실을 클릭시
        roomNumber01 = "4";
    }
    public void OnMidRoom5() // 2번 레벨의 5호실
    {
        roomNumber01 = "5";
    }
    public void OnMidRoom6() // 2번 레벨의 6호실
    {
        roomNumber01 = "6";
    }

    public void OnClickedHigh()
    {
        isTrueLow = false; // 1번 레벨은 끈다.
        isTrueHigh = true; // 3번레벨도 끈다.
        isTrueMid = false; // 2번레벨 킨다.
        imageFinal.SetActive(false);
        BackButton.SetActive(false);
        // DateTime.Now.ToString();
        LevelName01 = "03";

        imageLevel.gameObject.SetActive(false);
        imageRoom.gameObject.SetActive(true);

        LevelMid.gameObject.SetActive(false);
        LevelHigh.gameObject.SetActive(false);
        LevelLow.gameObject.SetActive(false);

        if (isTrueHigh == true)
        {
            HighRoomTotal.gameObject.SetActive(true);

        }

    }
    public void OnHighRoom1()
    { //3번 레벨의 1호실을 클릭시
        roomNumber01 = "1"; // 1이라는 숫자를 알려준다.
		L03_01 = true;
		Application.LoadLevel ("LevelMain");
    }
    public void OnHighRoom2()
    { // 3번레벨의 2호실을 클릭시
        roomNumber01 = "2";
		L03_02 = true;
		Application.LoadLevel ("LevelMain");
    }
    public void OnHighRoom3()
    { // 3번 레벨의 3호실을 클릭시
        roomNumber01 = "3";
		L03_03 = true;
		Application.LoadLevel ("LevelMain");
    }
    public void OnHighRoom4()
    { // 3번 레벨의 4호실을 클릭시
        roomNumber01 = "4";
    }

    void Awake()
    {  
        imageFinal.SetActive(imgFinalActive);
        BackButton.SetActive(backButtonActive);
    }

    // Use this for initialization
    void Start () {
        Screen.orientation = ScreenOrientation.Portrait; // 세로모드
        //Screen.orientation = ScreenOrientation.Landscape; // 가로모드
    }




    // Update is called once per frame
    void Update () {
        
	}
}
