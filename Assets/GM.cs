﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class GM : MonoBehaviour
{

    public GameObject step1, step2, wrong, timeOver, complete;
    public GameObject[] btn;
    public GameObject[] number;
    public Text[] text;
    public Image yellowImage, scoreImage;
    public Text timeText, scoreText;
    public int count = 0;
    public Image quiz, Level, Level_side, Level_bg;
    public Sprite L01_01, L01_02, L02_01, L02_02, L02_03, L03_01, L03_02, L03_03;
    public Sprite Level_01, Level_01_side, Level_02, Level_02_side, Level_03, Level_03_side;
    public Material bg;
    public Texture[] gameBg;
    public Sprite[] buttonColor;
    //DE83FF7D
    string[] L01_01Quiz = {"술기 전 손 씻기","물품준비하기 (비강캐뉼라, 산소유량계, 습윤병,증류수)","간호사 소개하기","대상자 확인하기","산소요법의 목적과 절차 설명하기"
                              ,"대상자에게 적절한 체위 취해주기 (반좌위)", "습윤병에 증류수를         눈금까지 채우기", "유량계와 습윤병을                      연결하여 Wall suction에                         연결하기", "   습윤병과 비강캐뉼라 연결하기", " O2를 켜서 비강캐뉼라에 산소가 나오는지 확인하기"
                              ,"대상자에게 비강캐뉼라 적용하기","  산소흡인량을 한번 더 확인하기","  대상자에게 코를 통해 호흡하는 것을 격려하기","마무리 손 씻기","간호기록지에 기록하기"};

    string[] L01_02Quiz = {"술기 전 손 씻기", "간호 술기(보호장비 착용 후        중환자 간호 제공하기)                        확인하기", "  필요한 물품을 준비하기(마스크, 가운, 멸균장갑)", "코와 입이 완전히 덮이도록 마스크 착용하기", "멸균가운의 포장지를 뜯고         목 가장자리를 잡고        공중에서 가운 펴기"
                              ,"가운에 머리에 쓰면서 입고 가운의 양 소매에 팔을 넣기", "무균상태 확보를 위해       왼손을 소매 속에 넣은 채      오른쪽 소매를 잡아당겨       손을 빼기", "무균적으로 허리띠를         풀어서 몸통에 돌려 감아      매기", "멸균장갑이 들어있는 종이포장을 펴기", "  왼손으로 오른손 장갑의 안쪽을 잡아 장갑 착용하기"
                              , "멸균상태인 오른손으로        왼쪽 장갑 겉 부분을 잡아서         왼쪽 장갑 착용하기", "격리실에 입실하기", "격리실에서 나올 때에는 허리끈을 풀어 양쪽으로 늘어뜨리기", "  장갑을 한쪽 장갑                    끝부분을 잡아 아래쪽을                       당겨서 벗기", " 남은 장갑도 안쪽을 잡아                 당겨 벗기"
                              ," 오른손 검지를 이용해                    왼쪽 소매 안쪽을 잡아                     가운이 어깨에서                    내려오도록 벗기", "  왼손 검지로도 오른쪽 팔 가운이   어깨 밑으로 내려오도록 벗기", "   가운의 안쪽만 손이       닿도록 하여 안전하게          벗기", "사용한 장갑과 가운은               감염성 폐기물 전용용기에                버리기", "마스크는 마지막으로 벗고,        벗은 마스크는 감염성        폐기물 전용용기에 버리기"
                              ,"   격리실에서 최종적으로    나오기 전, 손 씻기"};

    string[] L02_01Quiz = { "술기 전 손 씻기", "   투약처방 확인하기             (Nubain 1amp IM now)", "   필요한 물품 준비하기        (Nubain 1 ample, 2cc              주사기, 알코올솜)","Nubain 엠플 따기","2cc 주사기로 약물 재기"
                             ,"대상자에게 가서 간호사 소개하기","대상자 확인하기","근육주사의 목적과 절차 설명하기","손 위생 하기","    근육주사 위치 확인하기(배둔근)"
                             ,"주사 위치에 비우세 손 두 손가락으로 V자 만들기","V자 안쪽에 5~8mm 알코올           솜으로 안에서   밖으로 닦아내기","  우세손으로 주사기를 집어 뚜껑 제거하기","   주사바늘을 90도로 주사부위에 삽입하기","   주사기 잡은 손을 비우세손으로 바꾸기"
                             ,"우세손으로 주사기 밀대를      뒤로 당겨보기(regurgitation)","혈액 나오지 않는 것 확인        후 주사액 천천히 주입하기","소독솜을 주입부분에 가져가면서 주사 바늘    제거하기","주사부위 마사지하기","주사 바늘은 손상성폐기물 전용용기에 버리고 사용한     물품 정리하기"
                             ,"    대상자에게 주의사항 설명하기","술기 후 손 씻기","30분 정도 후에 근육주사      효과 확인하기","간호기록지에 기록하기"};

    string[] L02_02Quiz = {"술기 전 손 씻기","투약처방 확인하기 (Insulin       RI 2 u, SC, 9AM)","필요한 물품 준비하기(RI       Insulin 바이알, 인슐린        주사기, 알코올 솜)","  RI 인슐린 2u을 인슐린     주사기에 재기","대상자에게 가서 간호사 소개하기"
                         ,"대상자 확인하기","인슐린 주사의 목적과 절차 설명하기","손 위생 하기"," 피하주사 위치 확인하기      (삼각근 근처의 피부)","피하주사 할 피부를 알코올 솜으로 안에서 밖으로       5~8mm 닦아내기"
                         ," 우세손으로 주사기를 집어    뚜껑 제거하기","주사바늘을 45~90도로 주사부위에 삽입하기","  주사기 잡은 손을 비우세    손으로 바꾸기","우세손으로 천천히 주사액 주입하기","소독솜을 주입부분에 가져가면서 주사 바늘       제거하기"
                         ,"주사 바늘은 손상성폐기물 전용용기에 버리고 사용한    물품 정리하기"," 대상자에게 주의사항   설명하기","술기 후 손 씻기","간호기록지에 기록하기"};

    string[] L02_03Quiz = {"술기 전 손 씻기","의학 처방 (위관영양액         뉴케어 250cc 1캔 now via        L-tube) 확인하기","필요한 물품 준비하기        (뉴케어 캔 1개, 관장용        주사기,영양주입 용기와 물)","대상자에게 가서 간호사 소개하기","대상자 확인하기"
                        ,"위관 영양액 주입의 목적과    절차 설명하기","적절한 체위(30~45도 앉은       자세) 취해주기","손 위생 하기"," 영양주입 용기에 뉴케어    1캔을 담기","영양주입 용기 전체에                   뉴케어를 채워 공기                   제거하기"
                        ,"준비된 영양주입용기를 걸대(pole대)에 걸어두기","대상자의 위관을 마개부분을 꺾어 쥔 후, 마개를 빼기"," 30cc 공기를 채운 주사기를     위관 마개부분과 연결하기","꺾어 쥔 위관을 풀고 공기를 주입한 후 내용물             흡인해보기","흡인했던 내용물을 다시 넣어주기"
                         ,"위관 끝을 꺾어 쥔 후         위관과 주사기를 분리하기","위관 마개를 막기","주사기의 내관을 제거하기","다시 위관 끝부분을 꺾어        쥐고, 주사기 통과 위관        연결하기","실온의 물 30cc를 주사기에         넣어 중력에 의해 위관을          씻기(영양액 주입 전)"
                         ,"위관 끝부분을 꺾어 쥐고                       위관과 주사기 통을                      분리하기","위관영양주입용기를 위관 끝부분에 연결하기"," 처방된 영양액을 모두        주입하기","영양액이 다 들어가면                     용기의 clamp를 잠그고                           위관과 분리하기","위관 끝부분을 꺾어 쥐고       내관을 뺀 주사기 통을 다시          연결하기"
                        ,"실온의 물 30cc를 주사기에        넣어 중력에 의해 위관을          씻기(영양액 주입 후)","위관 끝부분을 꺾어 쥐고                                 위관과 주사기 통을                              분리하기"," 위관을 다시 제자리에       고정하기","대상자에게 현재의 체위를         30분 정도 유지할 것을       교육하기","사용한 물품 정리하기"
                         ,"술기 후 손 씻기","간호기록지에 기록하기"};

    string[] L03_01Quiz = {"술기 전 손 씻기","투약 처방(Cefotaxim 항생제 피부반응검사)과 투약원칙 확인하기","Cefotaxim 바이알 1g과 생리식염수를 사용하여      500:1 혹은 1000:1로         희석하기","희석한 용액을 피내주사용 주사기에 준비하기","대상자에게 가서 간호사 소개하기"
                         ,"대상자 확인하기","피내주사의 목적과 절차 설명하기","손 위생 하기","대상자의 팔을 편평한 곳에 노출하게 하고 편안한 자세 취하게 하기","주사할 부위를 알코올        솜으로 직경 5~8cm, 안에서      밖으로 닦기"
                         ,"소독 부위가 건조된 후 비우세손으로 주사부위       피부를 아래로 팽팽하게      당기기","주사 바늘 사면이 위로 오게 피부에 접촉한 후 진피에 주사바늘 삽입하기","주사 바늘이 5mm 정도        진피 아래로 보이도록             삽입하기","직경 1cm 정도되는 낭포가                     생길 때까지 약물을                주입하기","바늘 제거 후 약물이 스며 나오는 경우 알코올 솜으로 닦아주기"
                         ,"볼펜을 이용하여 낭포의       크기와 주사 시간 표시하기","주사 바늘은 손상성폐기물 전용용기에 버리고 사용한       물품 정리하기","술기 후 손 씻기","30분 정도 후에 항생제         피부반응 결과 확인하기","간호기록지에 기록하기"};

    string[] L03_02Quiz = {"술기 전 손씻기","  멸균적으로 유치도뇨세트      열기","세트 속 종지에 소독솜/멸균증류수 넣기","세트 속에 무균적으로 수용성                 윤활제(젤리) 투하하기","    세트 속에 멸균             주사기(10cc) 투하하기"
                         ," 적합한 크기의 도뇨관(카테터)을 세트에 무균적으로 넣기","대상자에게 가서 간호사 소개하기","대상자 확인하기","유치도뇨의 목적과 절차 설명하기","똑바로 눕는 체위 취해주기"
                         ,"대상자의 하의 탈의하고 배횡와위(dorsal recumbent position) 취해주기","손위생 실시하기","방수포를 대상자 둔부 밑에      깔기","준비한 세트를 대상자 다리 사이에 놓고 세트 열기","멸균장갑 끼기"
                         ,"멸균장갑 오염되지 않게      하면서 외음부를 공포(hole towel)로 덮기","  세트 내 주사기로             멸균증류수 3~5cc 재놓기","증류수가 담긴 주사기를              도뇨관에 연결하여 풍선 팽창                  확인하기","  도뇨관의 끝에 윤활제       바르고 섭자로 소변이            나오는 반대쪽 잠궈 두기","소독솜으로 외음부를        닦는다고 대상자에게      설명하기"
                         ,"왼손의 엄지와 검지로       대음순을 벌리고 도뇨관이       완전히 삽입될 때 까지         그대로 유지하기","외음부를 소독솜으로      소독하기 (오른손)","오른손으로 소음순을 소독솜으로 소독하기       (오른손)","요도 주위를 중앙에서 밖을 향하게 닦기 (오른손)","오른손으로 도뇨관을 잡고 요도에 5~8cm삽입하기"
                         ,"섭자를 잠시 열어서 소변이        나오는지 확인 수 다시                    잠궈 두기","양손으로 주사기에 있는      증류수 5cc 삽입하여 요도         내 풍선 부풀리기","도뇨관을 살짝 당겨서 요도      내 부풀린 풍선의 저항을       확인하기","공포를 제거한 후 도뇨관을       소변 수집 주머니와         연결하기","섭자를 제거하여 소변이 나오는지 확인하기"
                         ,"장갑을 벗고 반창고로           대퇴에 도뇨관을 고정하기","소변 주머니를 침상 옆에 고정하고 항상 몸 아래에 위치하도록 교육하기","세트와 공포, 방수포 등을 정리하기","대상자 주변을 정리하고 주의사항 전달하기","술기 후 손씻기"
                         ,"간호기록지에 기록하기"};

    string[] L03_03Quiz = {"술기 전 손 씻기","필요한 물품 준비하기(세트, 멸균증류수, 흡인관,         멸균장갑, 방포)","대상자에게 가서 간호사 소개하기","대상자 확인하기","벽에 있는 흡인기의 흡인압 확인하기(성인 110 -         150mmHg/ 아동 95 -        100mmHg)"
                         ,"흡인 카테터의 끝을 약간         열어 흡인기 관(PVC line)과      연결하기","대상자의 체위를 반좌위로 취해주기","대상자의 가슴에 수건이나      방포 얹기","필요한 세트를 열어      멸균증류수 담기","멸균 장갑을 무균적으로 착용하기"
                         ,"비우세손으로 카테터를           잡고 우세손을 이용해                 무균적으로 카테터 꺼내기","우세손으로 카테터를        윤활하고 비우세손으로           흡인 확인하기","카테터를 기관 내관 안으로 10cm정도 이상 삽입하기","압력을 걸고 카테터를      돌리면서 흡인해 내기","생리식염수를 통과시켜         관을 깨끗하게 하기"
                         ,"추가로 흡인이 필요한 경우         수 회 더 실시하기","흡인이 끝나면 우세손으로        카테터를 감싸 쥐면서             흡인기 관(PVC line)과            카테터를 분리하기","비우세손으로 흡인기 관            (PVC line)을 정리한 후 양손            장갑 벗기","대상자 호흡 양상 확인하고 주의사항 알려주기","대상자 주변을 정리하고       나오기"
                         ,"술기 후 손씻기","간호기록지에 기록하기"};
    string[] a;

    int delaytime = 2;
    Vector3[] position = new Vector3[5];
    float overTime;
    float overTimeCheck;
    float timeSecondcheck;
    float m;
    bool step2Start = false;
    float time1;
    public static int minTime;   //분
    public static int timeSecond;  //초
    public static float score = 100;
    public static string Completeprint = "";
    int cnt;

    // Use this for initialization
    void Start()
    {
        if (Levelbutton.L01_01 == true)
        {
            Level01_01();
        }
        else if (Levelbutton.L01_02 == true)
        {
            Level01_02();
        }
        else if (Levelbutton.L02_01 == true)
        {
            Level02_01();
        }
        else if (Levelbutton.L02_02 == true)
        {
            Level02_02();
        }
        else if (Levelbutton.L02_03 == true)
        {
            Level02_03();
        }
        else if (Levelbutton.L03_01 == true)
        {
            Level03_01();
        }
        else if (Levelbutton.L03_02 == true)
        {
            Level03_02();
        }
        else if (Levelbutton.L03_03 == true)
        {
            Level03_03();
        }
        bg.mainTexture = gameBg[6];
        score = 100;
        step1.SetActive(true);
        scoreText.text = score.ToString();
        cnt = a.Length - 1;
        overTimeCheck = overTime;
        if (overTime > 60)
        {
            m = overTime / 60;
            timeSecondcheck = overTime % 60;
            timeSecond = (int)timeSecondcheck;
        }
        else {
            m = 0;
            timeSecond = (int)overTime;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (step2Start == true)
        {
            time1 += Time.deltaTime;
            if (time1 > 1.0f)
            {
                timeSecond -= 1;
                overTime -= 1;
                minTime = (int)m;
                //timeText.text = "00:" + s.ToString ();
                timeText.text = string.Format("{0:D2}", minTime) + ":" + string.Format("{0:D2}", timeSecond);
                yellowImage.fillAmount = overTime / overTimeCheck;
                if (minTime <= 0 && timeSecond <= 0)
                {
                    step2Start = false;
                    timeOver.SetActive(true);
                    StartCoroutine(sendFail());
                }
                if (timeSecond <= 0)
                {
                    m--;
                    timeSecond = 60;
                }
                time1 = 0;
            }
            if (number[cnt].active == true)
            {                   //입원실 구분
                step2Start = false;
                complete.SetActive(true);
                Completeprint = "Complete";
            }
        }

    }

    public void StartClick1()
    {
        step1.SetActive(false);
        step2.SetActive(true);
        step2Start = true;
        int r = UnityEngine.Random.Range(0, 5);
        print(r);
        bg.mainTexture = gameBg[r];
        Quiz();
    }

    public void QuizClick1()
    {
        if (number[0].active == false)
        {
            number[0].SetActive(true);
            Destroy(GameObject.Find("Button1(Clone)"));
        }
        else if (number[5].active == false && number[4].active == true)
        {
            number[5].SetActive(true);
            Destroy(GameObject.Find("Button1(Clone)"));
        }
        else if (number[10].active == false && number[9].active == true)
        {
            number[10].SetActive(true);
            Destroy(GameObject.Find("Button1(Clone)"));
        }
        else if (number[15].active == false && number[14].active == true)
        {
            number[15].SetActive(true);
            Destroy(GameObject.Find("Button1(Clone)"));
        }
        else if (number[20].active == false && number[19].active == true)
        {
            number[20].SetActive(true);
            Destroy(GameObject.Find("Button1(Clone)"));
        }
        else if (number[25].active == false && number[24].active == true)
        {
            number[25].SetActive(true);
            Destroy(GameObject.Find("Button1(Clone)"));
        }
        else if (number[30].active == false && number[29].active == true)
        {
            number[30].SetActive(true);
            Destroy(GameObject.Find("Button1(Clone)"));
        }
        else if (number[35].active == false && number[34].active == true)
        {
            number[35].SetActive(true);
            Destroy(GameObject.Find("Button1(Clone)"));
        }
        else
        {
            StartCoroutine(wrongAnser());
        }
    }
    public void QuizClick2()
    {
        if (number[1].active == false && number[0].active == true)
        {
            number[1].SetActive(true);
            Destroy(GameObject.Find("Button2(Clone)"));
        }
        else if (number[6].active == false && number[5].active == true)
        {
            number[6].SetActive(true);
            Destroy(GameObject.Find("Button2(Clone)"));
        }
        else if (number[11].active == false && number[10].active == true)
        {
            number[11].SetActive(true);
            Destroy(GameObject.Find("Button2(Clone)"));
        }
        else if (number[16].active == false && number[15].active == true)
        {
            number[16].SetActive(true);
            Destroy(GameObject.Find("Button2(Clone)"));
        }
        else if (number[21].active == false && number[20].active == true)
        {
            number[21].SetActive(true);
            Destroy(GameObject.Find("Button2(Clone)"));
        }
        else if (number[26].active == false && number[25].active == true)
        {
            number[26].SetActive(true);
            Destroy(GameObject.Find("Button2(Clone)"));
        }
        else if (number[31].active == false && number[30].active == true)
        {
            number[31].SetActive(true);
            Destroy(GameObject.Find("Button2(Clone)"));
        }
        else if (number[36].active == false && number[35].active == true)
        {
            number[36].SetActive(true);
            Destroy(GameObject.Find("Button2(Clone)"));
        }
        else
        {
            StartCoroutine(wrongAnser());
        }
    }
    public void QuizClick3()
    {
        if (number[2].active == false && number[1].active == true)
        {
            number[2].SetActive(true);
            Destroy(GameObject.Find("Button3(Clone)"));
        }
        else if (number[7].active == false && number[6].active == true)
        {
            number[7].SetActive(true);
            Destroy(GameObject.Find("Button3(Clone)"));
        }
        else if (number[12].active == false && number[11].active == true)
        {
            number[12].SetActive(true);
            Destroy(GameObject.Find("Button3(Clone)"));
        }
        else if (number[17].active == false && number[16].active == true)
        {
            number[17].SetActive(true);
            Destroy(GameObject.Find("Button3(Clone)"));
        }
        else if (number[22].active == false && number[21].active == true)
        {
            number[22].SetActive(true);
            Destroy(GameObject.Find("Button3(Clone)"));
        }
        else if (number[27].active == false && number[26].active == true)
        {
            number[27].SetActive(true);
            Destroy(GameObject.Find("Button3(Clone)"));
        }
        else if (number[32].active == false && number[31].active == true)
        {
            number[32].SetActive(true);
            Destroy(GameObject.Find("Button3(Clone)"));
        }
        else if (number[37].active == false && number[36].active == true)
        {
            number[37].SetActive(true);
            Destroy(GameObject.Find("Button3(Clone)"));
        }
        else
        {
            StartCoroutine(wrongAnser());
        }

    }
    public void QuizClick4()
    {
        if (number[3].active == false && number[2].active == true)
        {
            number[3].SetActive(true);
            Destroy(GameObject.Find("Button4(Clone)"));
        }
        else if (number[8].active == false && number[7].active == true)
        {
            number[8].SetActive(true);
            Destroy(GameObject.Find("Button4(Clone)"));
        }
        else if (number[13].active == false && number[12].active == true)
        {
            number[13].SetActive(true);
            Destroy(GameObject.Find("Button4(Clone)"));
        }
        else if (number[18].active == false && number[17].active == true)
        {
            number[18].SetActive(true);
            Destroy(GameObject.Find("Button4(Clone)"));
        }
        else if (number[23].active == false && number[22].active == true)
        {
            number[23].SetActive(true);
            Destroy(GameObject.Find("Button4(Clone)"));
        }
        else if (number[28].active == false && number[27].active == true)
        {
            number[28].SetActive(true);
            Destroy(GameObject.Find("Button4(Clone)"));
        }
        else if (number[33].active == false && number[32].active == true)
        {
            number[33].SetActive(true);
            Destroy(GameObject.Find("Button4(Clone)"));
        }
        else
        {
            StartCoroutine(wrongAnser());
        }
    }
    public void QuizClick5()
    {
        if (number[4].active == false && number[3].active == true)
        {
            Quiz();
            number[4].SetActive(true);
            Destroy(GameObject.Find("Button5(Clone)"));                                                            //
        }
        else if (number[9].active == false && number[8].active == true)
        {
            Quiz();
            number[9].SetActive(true);
            Destroy(GameObject.Find("Button5(Clone)"));
        }
        else if (number[14].active == false && number[13].active == true)
        {
            Quiz();
            number[14].SetActive(true);
            Destroy(GameObject.Find("Button5(Clone)"));
        }
        else if (number[19].active == false && number[18].active == true)
        {
            Quiz();
            number[19].SetActive(true);
            Destroy(GameObject.Find("Button5(Clone)"));
        }
        else if (number[24].active == false && number[23].active == true)
        {
            Quiz();
            number[24].SetActive(true);
            Destroy(GameObject.Find("Button5(Clone)"));
        }
        else if (number[29].active == false && number[28].active == true)
        {
            Quiz();
            number[29].SetActive(true);
            Destroy(GameObject.Find("Button5(Clone)"));
        }
        else if (number[34].active == false && number[33].active == true)
        {
            Quiz();
            number[34].SetActive(true);
            Destroy(GameObject.Find("Button5(Clone)"));
        }
        else
        {
            StartCoroutine(wrongAnser());
        }
    }

    public void Regame()
    {
        //다시하기 
        timeOver.SetActive(false);
        Application.LoadLevel("LevelMain");
    }

    public void Home()
    {
        //홈으로 
        timeOver.SetActive(false);
        Levelbutton.L01_01 = false;
        Levelbutton.L01_02 = false;
        Levelbutton.L02_01 = false;
        Levelbutton.L02_02 = false;
        Levelbutton.L02_03 = false;
        Levelbutton.L03_01 = false;
        Levelbutton.L03_02 = false;
        Levelbutton.L03_03 = false;
        Application.LoadLevel("LevelSelect");
    }
    public void Complete()
    {
        //확인 
        Application.LoadLevel("TotalScene");
        if (score == 100)
        {
            score += 100;
        }
        print(score + "," + minTime + ":" + timeSecond);

    }

    void Quiz()
    {
        position[0] = new Vector3(-45, 30, 0);
        position[1] = new Vector3(30, 25, 0);
        position[2] = new Vector3(-4, -7, 0);
        position[3] = new Vector3(-45, -40, 0);
        position[4] = new Vector3(50, -25, 0);
        Button btn1;
        bool[] check = new bool[5];

        for (int i = 0; i < 5;)
        {
            int n = UnityEngine.Random.Range(0, 5);
            //int n=i;
            if (check[n] == false)
            {
                check[n] = true;
                if (count == a.Length) return;
                text[i].text = a[count];
                GameObject bt = Instantiate(btn[i]);

                bt.transform.SetParent(step2.transform); //부모설정
                bt.transform.localPosition = position[n];
                bt.transform.localRotation = Quaternion.Euler(0, 0, 0);
                bt.transform.localScale = new Vector3(1, 1, 1);

                if (Levelbutton.L01_01 == true || Levelbutton.L01_02 == true) {
                    bt.GetComponent<Image>().sprite = buttonColor[0];
                }
                else if (Levelbutton.L02_01 == true || Levelbutton.L02_02 == true || Levelbutton.L03_02 == true)
                {
                    bt.GetComponent<Image>().sprite = buttonColor[1];
                }
                else
                {
                    bt.GetComponent<Image>().sprite = buttonColor[2];
                }
                

                bt.transform.SetSiblingIndex(5);

                if (i == 0)
                {
                    btn1 = (Button)bt.GetComponent<Button>();
                    btn1.onClick.AddListener(() => QuizClick1());
                }
                else if (i == 1)
                {
                    btn1 = (Button)bt.GetComponent<Button>();
                    btn1.onClick.AddListener(() => QuizClick2());
                }
                else if (i == 2)
                {
                    btn1 = (Button)bt.GetComponent<Button>();
                    btn1.onClick.AddListener(() => QuizClick3());
                }
                else if (i == 3)
                {
                    btn1 = (Button)bt.GetComponent<Button>();
                    btn1.onClick.AddListener(() => QuizClick4());
                }
                else if (i == 4)
                {
                    btn1 = (Button)bt.GetComponent<Button>();
                    btn1.onClick.AddListener(() => QuizClick5());
                }



                i++;
                count++;
            }
        }
        /*
         for (int i = 0; i < 5;)
        {
            int n = Random.Range(0, 5);
            if (check[n] == false)
            {
                check[n] = true;
                text[n].text = a[count];
                i++;
                count++;
            }
        }
        */

    }

    void Level01_01()
    {
        a = L01_01Quiz;
        quiz.sprite = L01_01;
        Level.sprite = Level_01;
        Level_side.sprite = Level_01_side;
        Level_bg.color = new Color32(223, 131, 255, 125);
        overTime = 40;
    }
    void Level01_02()
    {
        a = L01_02Quiz;
        quiz.sprite = L01_02;
        Level.sprite = Level_01;
        Level_side.sprite = Level_01_side;
        Level_bg.color = new Color32(223, 131, 255, 125);
        overTime = 52;
    }
    void Level02_01()
    {
        a = L02_01Quiz;
        quiz.sprite = L02_01;
        Level.sprite = Level_02;
        Level_side.sprite = Level_02_side;
        Level_bg.color = new Color32(136, 131, 255, 125);
        overTime = 58;
    }
    void Level02_02()
    {
        a = L02_02Quiz;
        quiz.sprite = L02_02;
        Level.sprite = Level_02;
        Level_side.sprite = Level_02_side;
        Level_bg.color = new Color32(136, 131, 255, 125);
        overTime = 38;
    }
    void Level02_03()
    {
        a = L02_03Quiz;
        quiz.sprite = L02_03;
        Level.sprite = Level_02;
        Level_side.sprite = Level_02_side;
        Level_bg.color = new Color32(136, 131, 255, 125);
        overTime = 74;
    }
    void Level03_01()
    {
        a = L03_01Quiz;
        quiz.sprite = L03_01;
        Level.sprite = Level_03;
        Level_side.sprite = Level_03_side;
        Level_bg.color = new Color32(136, 255, 255, 125);
        overTime = 50;
    }
    void Level03_02()
    {
        a = L03_02Quiz;
        quiz.sprite = L03_02;
        Level.sprite = Level_03;
        Level_side.sprite = Level_03_side;
        Level_bg.color = new Color32(136, 255, 255, 125);
        overTime = 82;
    }
    void Level03_03()
    {
        a = L03_03Quiz;
        quiz.sprite = L03_03;
        Level.sprite = Level_03;
        Level_side.sprite = Level_03_side;
        Level_bg.color = new Color32(136, 255, 255, 125);
        overTime = 54;
    }

   /* IEnumerator delay()
    {
        yield return new WaitForSeconds(delaytime);
        
    }
    */
    IEnumerator wrongAnser()
    {
        wrong.SetActive(true);
        yield return new WaitForSeconds(delaytime);
        wrong.SetActive(false);
        score -= 5;
        scoreImage.fillAmount = score / 100;
        scoreText.text = score.ToString();
        if (score <= 0)
        {
            step2Start = false;
            timeOver.SetActive(true);
            Completeprint = "failure";

            StartCoroutine(sendFail());
        }
    }

    IEnumerator sendFail()
    {

        WWWForm cForm = new WWWForm();
        cForm.AddField("idnum", DB.idnum);
        cForm.AddField("method", "ud");
        cForm.AddField("token", DB.token);
        cForm.AddField("diff", Convert.ToInt16(Levelbutton.LevelName01));
        cForm.AddField("level", Convert.ToInt16(Levelbutton.roomNumber01));
        cForm.AddField("score", 0);
        cForm.AddField("success", 0);
        cForm.AddField("duration", GM.minTime * 60 + GM.timeSecond);
        WWW wwwUrl = new WWW("http://ciiwolstudio.com/nursetrain/communicate.php", cForm);
        yield return wwwUrl;

        if (wwwUrl.error == null)
        {
            Debug.Log(wwwUrl.text);
        }
        else
        {
            Debug.Log("WWW error :" + wwwUrl.error);
        }
    }
}
