﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

	int a=0;
	int p;

	// Use this for initialization
	void Start () {
		if (gameObject.transform.localPosition.y >= 30 && gameObject.transform.localPosition.y <= 31) {
			p=0;
		}
		else if (gameObject.transform.localPosition.y >= 25 && gameObject.transform.localPosition.y <= 26) {
			p=1;
		}
		else if (gameObject.transform.localPosition.y >= -8 && gameObject.transform.localPosition.y <= -7) {
			p=2;
		}
		else if (gameObject.transform.localPosition.y >= -41 && gameObject.transform.localPosition.y <= -40) {
			p=3;
		}
		else if (gameObject.transform.localPosition.y >= -26 && gameObject.transform.localPosition.y <= -25) {
			p=4;
		}
        StartCoroutine(time());
	}
	
	// Update is called once per frame
	void Update () {
		if (p == 0) {
			if (a == 0) {
				gameObject.GetComponent<Transform> ().Translate (0.2f, 0, 0);
			}
			else if (a == 1) {
				gameObject.GetComponent<Transform> ().Translate (0, 0.2f, 0);
			}
			else if (a == 2) {
				gameObject.GetComponent<Transform> ().Translate (-0.2f, 0, 0);
			}
			else if (a == 3) {
				gameObject.GetComponent<Transform> ().Translate (0, -0.2f, 0);
			}
		}
		if (p == 1) {
			if (a == 0) {
				gameObject.GetComponent<Transform> ().Translate (0.1f, 0.1f, 0);
			}
			else if (a == 1) {
				gameObject.GetComponent<Transform> ().Translate (-0.2f, 0.2f, 0);
			}
			else if (a == 2) {
				gameObject.GetComponent<Transform> ().Translate (0.2f, -0.2f, 0);
			}
			else if (a == 3) {
				gameObject.GetComponent<Transform> ().Translate (-0.1f, -0.1f, 0);
			}
		}
		if (p == 2) {
			if (a == 0) {
				gameObject.GetComponent<Transform> ().Translate (0, -0.1f, 0);
			}
			else if (a == 1) {
				gameObject.GetComponent<Transform> ().Translate (0, 0.1f, 0);
			}
			else if (a == 2) {
				gameObject.GetComponent<Transform> ().Translate (-0.1f, 0, 0);
			}
			else if (a == 3) {
				gameObject.GetComponent<Transform> ().Translate (0.1f, 0, 0);
			}
		}
		if (p == 3) {
			if (a == 0) {
				gameObject.GetComponent<Transform> ().Translate (0.2f, 0, 0);
			}
			else if (a == 1) {
				gameObject.GetComponent<Transform> ().Translate (-0.2f, 0, 0);
			}
			else if (a == 2) {
				gameObject.GetComponent<Transform> ().Translate (0, 0.2f, 0);
			}
			else if (a == 3) {
				gameObject.GetComponent<Transform> ().Translate (0, -0.2f, 0);
			}
		}
		if (p == 4) {
			if (a == 0) {
				gameObject.GetComponent<Transform> ().Translate (0.1f, 0.1f, 0);
			}
			else if (a == 1) {
				gameObject.GetComponent<Transform> ().Translate (-0.1f, -0.1f, 0);
			}
			else if (a == 2) {
				gameObject.GetComponent<Transform> ().Translate (0.1f, 0.1f, 0);
			}
			else if (a == 3) {
				gameObject.GetComponent<Transform> ().Translate (-0.1f, -0.1f, 0);
			}
		}
	}

	IEnumerator time(){
		while (true) {
			yield return new WaitForSeconds (0.2f);
			a = 1;
			yield return new WaitForSeconds (0.2f);
			a = 2;
			yield return new WaitForSeconds (0.2f);
			a = 3;
			yield return new WaitForSeconds (0.2f);
			a = 0;
		}
	}
}
